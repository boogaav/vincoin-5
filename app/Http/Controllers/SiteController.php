<?php

namespace App\Http\Controllers;

use App\Services\RewardSystem\RewardHelper;
use App\User;
use App\Wallet;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ContactFormRequest;
use App\Mail\ContactUsMail;
use App\ReferralLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware("web");
    }

    public function index()
    {
        $wallets = Wallet::all()->keyBy('name');
//        $win_wallet = Wallet::where('name', 'win-pc')->first();
//        $mac_wallet = Wallet::where('name', 'mac-os')->first();
//        $linux_wallet = Wallet::where('name', 'linux')->first();
//        $github_wallet = Wallet::where('name', 'github')->first();
        return view("pages.home", ['wallets' => $wallets]);
    }

    public function styleGuide()
    {
        return view("pages.style-guide");
    }

    public function contactUsProcess(ContactFormRequest $request)
    {
        // validation success, send mail
        $to = config('admin.admin_email');
        Mail::to($to)->send(new ContactUsMail($request));
    }

    public function showAccountPage()
    {
        $user = Auth::user();
        $ref_link = ReferralLink::where('referrer_id', $user['id'])->first();
        return view("pages.account", ['user' => $user, 'ref_link' => $ref_link]);
    }

    public function updateAccount(Request $request)
    {
            $user = Auth::user();
            $acc_updated = false;
            $tokens_added = false;

            if ($request->filled("wallet_number")) {
                $user->profile->wallet_number = $request->get("wallet_number");
                $acc_updated = true;
            }

            if ($user->profile->is_telegram_joined === false && $request->filled("telegram_nickname")) {
                $user->profile->telegram_nickname = $request->get("telegram_nickname");
                $user->profile->is_telegram_joined = true;
//                RewardHelper::addBonusTokensTo($user->id, RewardHelper::BONUS_AMOUNT_B);
                $acc_updated = true;
                $tokens_added = true;
            } elseif (!empty($user->profile->telegram_nickname) && $user->profile->is_telegram_joined === true && $request->filled("telegram_nickname")) {
                $user->profile->telegram_nickname = $request->get("telegram_nickname");
                $acc_updated = true;
            } elseif ($user->profile->is_facebook_joined === false && $request->get("groupType") === 'facebook') {
                $user->profile->is_facebook_joined = true;
//                RewardHelper::addBonusTokensTo($user->id, RewardHelper::BONUS_AMOUNT_A);
                $acc_updated = true;
                $tokens_added = true;
            } elseif ($user->profile->is_twitter_joined === false && $request->get("groupType") === 'twitter') {
                $user->profile->is_twitter_joined = true;
//                RewardHelper::addBonusTokensTo($user->id, RewardHelper::BONUS_AMOUNT_A);
                $acc_updated = true;
                $tokens_added = true;
            }

            $tokens_added = false;

            $user->profile->save();

            return response()->json([
                'acc_updated' => $acc_updated,
                'tokens_added' => $tokens_added
            ]);
    }

    public function smartContract()
    {
        return view("pages.smart-contract");
    }

    public function trackWallets(Request $request)
    {
        Wallet::find($request->get('wallet_id'))->increment('downloads_count', 1);
    }
}

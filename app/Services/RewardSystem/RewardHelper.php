<?php

namespace App\Services\RewardSystem;

use App\UserProfile;

class RewardHelper
{
    const BONUS_AMOUNT_A = 25;
    const BONUS_AMOUNT_B = 50;

    public static function addBonusTokensTo($user_id, $amount)
    {
        return UserProfile::where(['user_id' => $user_id])->increment("bonus_tokens", $amount);
    }
}

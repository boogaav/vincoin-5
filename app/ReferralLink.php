<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;

class ReferralLink extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'referral_links';

    protected $casts = [
        'expired' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['url'];

    public static function generateUrl($ref_id)
    {
//        return url("/") . '?ref_id=' . self::generateRefId($ref_id);
        return route("auth.showLogin") . '?ref_id=' . self::generateRefId($ref_id);
    }

    public static function generateRefId($user_id, $decode = false)
    {
        $hashId = new Hashids('', 5);
        return !$decode ? $hashId->encode($user_id) : $hashId->decode($user_id);
    }
}

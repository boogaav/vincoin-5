
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');

// require page scripts
require('./pages/auth');
require('./pages/account');
const Ladda = require("ladda");
const ClipboardJS = require("../vendor/clipboard/clipboard.min");

// loading button init
Ladda.bind( '.ladda-button');
window.Ladda = Ladda;

import Form from 'form-backend-validation';
import FormHelper from './utils/form-helper';

$(function() {
    let mainHeader = $(".main-header"),
        scrollUpBtn = $(".scroll-up-btn"),
        telegramGroupBtn = $('.telegram-support'),
        airDropNotify = $(".airDrop-notify"),
        firstSectionHeight = $(".section-intro").outerHeight(true),
        // headerHeight = mainHeader.outerHeight(),
        prevScrollPos = window.pageYOffset,
        currentScrollPos = window.pageYOffset;

    let scrolling = false,
        previousTop = 0,
        currentTop = 0,
        scrollDelta = 10,
        scrollOffset = 300;

    $('body').addClass('i-loaded');
    document.addEventListener("touchstart", function() {},false);
    // if (!("ontouchstart" in document.documentElement)) {
    //     document.documentElement.className += " no-touch";
    // } else {
    //     document.documentElement.className += " touch";
    // }

    if (currentScrollPos < scrollOffset) {
        scrollUpBtn.addClass("hidden");
        telegramGroupBtn.addClass("hidden");
        // airDropNotify.addClass("hidden");
    } else {
        scrollUpBtn.removeClass("hidden");
        telegramGroupBtn.removeClass("hidden");
        // airDropNotify.removeClass("hidden");
    }
    /* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */

    $(window).on('scroll', function(){
        if( !scrolling ) {
            scrolling = true;
            (!window.requestAnimationFrame)
                ? setTimeout(autoHideHeader, 250)
                : requestAnimationFrame(autoHideHeader);
        }
    });

    function autoHideHeader() {
        let currentTop = $(window).scrollTop();

        checkSimpleNavigation(currentTop);

        previousTop = currentTop;
        scrolling = false;
    }

    function checkSimpleNavigation(currentTop) {
        //there's no secondary nav or secondary nav is below primary nav
        if (previousTop - currentTop > scrollDelta) {
            //if scrolling up...
            mainHeader.removeClass('is-minimized');
            scrollUpBtn.removeClass("hidden");
        } else if( currentTop - previousTop > scrollDelta && currentTop > scrollOffset && window.innerWidth <= 991) {
            //if scrolling down...
            mainHeader.addClass('is-minimized');
            scrollUpBtn.addClass("hidden");
        }
        if (currentTop < scrollOffset) {
            scrollUpBtn.addClass("hidden");
            telegramGroupBtn.addClass("hidden");
        } else {
            scrollUpBtn.removeClass("hidden");
            telegramGroupBtn.removeClass("hidden");
        }
    }

    // window.onscroll = function() {
    //     let currentScrollPos = window.pageYOffset;
    //     if (prevScrollPos < currentScrollPos && currentScrollPos > 300 && window.innerWidth <= 991) {
    //         mainHeader.addClass("is-minimized");
    //         scrollUpBtn.addClass("hidden");
    //         // airDropNotify.addClass("hidden");
    //     } else {
    //         mainHeader.removeClass("is-minimized");
    //         scrollUpBtn.removeClass("hidden");
    //         // airDropNotify.removeClass("hidden");
    //     }
    //     if (currentScrollPos < 300) {
    //         scrollUpBtn.addClass("hidden");
    //         // airDropNotify.addClass("hidden");
    //     }
    //     prevScrollPos = currentScrollPos;
    // };

    scrollUpBtn.on('click', function (e) {
        $('html, body').stop().animate({
            scrollTop: 0
        }, 400);
    });

    new ClipboardJS('.copy-btn');

    $('.js-nav-trigger').on('click', function (event) {
        // $('.main-wrap').toggleClass('nav-opened');
        toggleMobileMenu();
    });

    $('.nav-mobile .nav__link.link-anchor').on('click', function (e) {
        toggleMobileMenu();
    });

    $('.link-anchor').on('click', function (event) {
        event.preventDefault();

        if (! $('body').hasClass("fancybox-active")) {
            //Animate scroll
            $('html, body').stop().animate({
                scrollTop: $( $(this).attr('href') ).offset().top - 75
            }, 400);
        }
    });

    const $contactForm = $('.contact-us-form');

    FormHelper.initFormModal($contactForm);

    $contactForm.on('submit', function (event) {
        event.preventDefault();

        const $form = $(this),
              $nameField = $form.find('input[name="name"]'),
              $emailField = $form.find('input[name="email"]'),
              $msgField = $form.find('textarea[name="message"]'),
              postUrl = $form.attr('action');

        FormHelper.hideErrors($form);
        FormHelper.processingForm($form, true);

        const contactForm = new Form({
            name: $nameField.val(),
            email: $emailField.val(),
            message: $msgField.val(),
        });

        contactForm.post(postUrl)
            .then(function (response) {
                Ladda.stopAll();
                FormHelper.processingForm($form, false);
                FormHelper.showSuccessMsg($form, function ($form) {
                    setTimeout(function () {
                        $.fancybox.getInstance().close();
                    }, 4000);
                });
            })
            .catch(function (error) {
                Ladda.stopAll();
                FormHelper.processingForm($form, false);
                FormHelper.showErrors($form, contactForm);
                console.log(error.response.data);
            });
    });


    // $('.lazy').Lazy();
    $('.lazy').Lazy({
        beforeLoad: function(element) {
            // called before an elements gets handled
        },
        afterLoad: function(element) {
            // called after an element was successfully handled
            if (element.hasClass('js-delayed-section')) {
                // console.log('delayed-section');
                showDelayedBlocks($(element).parent(), 300);
            }
        },
        onError: function(element) {
            // called whenever an element could not be handled
        },
        onFinishedAll: function() {
            // called once all elements was handled
        }
    });

    // $('.js-select-language').attr('disabled', false);
    //
    // $('.js-select-language').on('change', function (event) {
    //     event.preventDefault();
    //
    //     window.location.href = this.value;
    // });

    $('.js-wallet-download').on('click', function (event) {
        const walletsTrackRequest = new Form({
            wallet_id: $(this).data('wallet-id'),
        });


        walletsTrackRequest.post('/wallets/download')
            .then(function (response) {

            })
            .catch(function (error) {

            });
    });

    initMLMenu();

    $("#ml-menu .menu__level--current .menu__link[data-submenu], #ml-menu .menu__back").on('click', function (event) {
        $('#ml-menu').parent().stop().animate({
            scrollTop: 0
        }, 700);
    });

    // Tabs
    $('.tabs__controls li').on('click', function (event) {
        event.preventDefault();
        let currTabId = $(this).find('a').attr('href');

        $('.tabs__controls li').removeClass('active');
        $(this).addClass('active');

        $('.tab').removeClass('active');
        $(currTabId).addClass('active');
    });
});

function toggleMobileMenu() {
    $('body').toggleClass('nav-opened');
    if ($('body').hasClass('nav-opened')) {
        calcMobileMenuHeight();
        $(".notification-group, .notification").addClass("hidden");
    } else {
        // $(".airDrop-notify").removeClass("hidden");
        $(".notification-group, .notification").removeClass("hidden");
    }
}

function initMLMenu() {
    let menuEl = document.getElementById('ml-menu'),
        mlmenu = new MLMenu(menuEl, {
            breadcrumbsCtrl : false, // show breadcrumbs
            // initialBreadcrumb : 'all', // initial breadcrumb text
            backCtrl : false, // show back button
            customBackCtrl: '.submenu-back',
            // itemsDelayInterval : 60, // delay between each menu item sliding animation
            onItemClick: function () {
                // console.log("Item clicked.");
            } // callback: item that doesn´t have a submenu gets clicked - onItemClick([event], [inner HTML of the clicked item])
        });
}

function calcMobileMenuHeight() {
    let menuHeight = 0;
    let defaultMenuHeight = 288;
    $("#ml-menu").find(".menu__level[data-menu='main'] .menu__item").each(function () {
        menuHeight += $(this).outerHeight(true);
        // console.log("itemHeight", $(this).outerHeight(true));
    });
    if (menuHeight > defaultMenuHeight) {
        $("#ml-menu").height(menuHeight);
    } else {
        $("#ml-menu").height(defaultMenuHeight);
    }
    // console.log("menuHeight", menuHeight);
}

function showDelayedBlocks($section, initDelaySec) {
    let delaySec = initDelaySec * 2;
    // console.log('section: ', $section);
    $section.find('.delayed').each(function (i, elem) {
        let $elem = $(this);
        // console.log('elem', $elem);
        if (i === 0) {
            delaySec = initDelaySec;
        }
        let timerId = setTimeout(function tick() {
            // $elem.show();
            $elem.removeClass('delayed-hidden');
            timerId = setTimeout(tick, delaySec); // (*)
        }, delaySec);
        delaySec += initDelaySec;
    })
}

// particlesJS.load('particles-js--header', "vendor/particles.json", function() {
//     console.log('callback - particles.js config loaded');
// });
//
// particlesJS.load('particles-js--structure', "vendor/particles.json", function() {
//     console.log('callback - particles.js config loaded');
// });
//
// particlesJS.load('particles-js--roadmap', "vendor/particles.json", function() {
//     console.log('callback - particles.js config loaded');
// });

// Table toggle
// $(".vin-table-toggle").on('click', function (event) {
//     event.preventDefault();
//     $('.vin-table-holder').toggle();
// });

// Accordion
$('.toggle').on('click', function(e) {
    e.preventDefault();

    const $this = $(this);

    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
        $this.parents('.accordion__item').removeClass('active');
    } else {
        $this.parent().parent().find('li .inner').removeClass('show');
        $this.parent().parent().find('li .inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.parent().parent().find('.accordion__item').removeClass('active');
        $this.parent().addClass('active');
        $this.next().slideToggle(350);
    }
});

// Timeline
$('.js-timeline').Timeline({
    // autoplay: true,
    mode: 'vertical',
    itemClass: 'box-item',
    startItem: 9
});
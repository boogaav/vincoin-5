
const FormHelper = {
    initFormModal($form) {
        const self = this;
        $("[data-src='#contact-us-modal']").fancybox({
            beforeShow: function( instance, slide ) {

                $('body').removeClass('nav-opened');
                if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) <= 576
                    && $(document).scrollTop() < 40
                    && ! $('.main-header').hasClass("js-main-header--default")) {
                    $('.main-header').removeClass("main-header--transparent");
                }
                // reset contact us tabs
                $(".contact-us__tabs .tabs__controls__item-left .link").click();
                // Add class to body to indicate that contact-us is open
                $('body').addClass("contact-us-active");

                self.resetForm($form);

                // Tip: Each event passes useful information within the event object:

                // Object containing references to interface elements
                // (background, buttons, caption, etc)
                // console.info( instance.$refs );

                // Current slide options
                // console.info( slide.opts );

                // Clicked element
                // console.info( slide.opts.$orig );

                // Reference to DOM element of the slide
                // console.info( slide.$slide );

            },
            afterShow: function (instance, slide) {
                self.focusField($form, 'name');
                // slide.opts.$orig.removeAttr("data-src");
            },
            afterClose: function (instance, slide) {
                // slide.opts.$orig.setAttribute("data-src", '#contact-us-modal');
                if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) <= 576
                    && $(document).scrollTop() < 40
                    && ! $('.main-header').hasClass("js-main-header--default")) {
                    $('.main-header').addClass("main-header--transparent");
                }
                $('body').removeClass("contact-us-active");
                $('.notification-group, .notification').removeClass("hidden");
            }
        });
    },
    showErrors($form, Form) {
        let allErrors = Form.errors.all();
        for (let field in allErrors) {
            if (allErrors.hasOwnProperty(field)) {
                $form.find('.help-block[data-for="'+ field +'"]').text(Form.errors.first(field)).parent().addClass('has-error');
            }
        }
    },
    hideErrors($form) {
        $form.find('.has-error').removeClass('has-error').find('.help-block').text('');
    },
    processingForm($form, status) {
        if (status === true) {
            $form.addClass('processing');
            $form.find('input, textarea').attr('readonly', true);
            $form.find('.submit-btn').attr('disabled', true);
        } else {
            $form.removeClass('processing');
            $form.find('input, textarea').attr('readonly', false);
            $form.find('.submit-btn').attr('disabled', false);
        }
    },
    showSuccessMsg($form, callback) {
        const self = this;
        $form.parents('.js-form-container').find('.js-form-content').hide();
        $form.parents('.js-form-container').find('.js-form-success-msg').show();
        if (callback !== undefined) callback($form);
    },
    resetForm($form, delay, callback) {
        const self = this;
        if (delay) {
            setTimeout(function () {
                $form.find('input, textarea').val('');
                $form.parents('.js-form-container').find('.js-form-content').show();
                $form.parents('.js-form-container').find('.js-form-success-msg').hide();
                self.hideErrors($form);
            }, delay);
        } else {
            $form.find('input, textarea').val('');
            $form.parents('.js-form-container').find('.js-form-content').show();
            $form.parents('.js-form-container').find('.js-form-success-msg').hide();
            self.hideErrors($form);
        }
        if (callback !== undefined) callback($form);
    },
    showField($form, fieldName) {
        $form.find(fieldName).attr('disabled', false).parents(".form-group").show();
    },
    hideField($form, fieldName) {
        $form.find(fieldName).attr('disabled', true).parents(".form-group").hide();
    },
    focusField($form, fieldName) {
        $form.find('input[name="' + fieldName + '"]').focus();
    }
};

export default FormHelper;

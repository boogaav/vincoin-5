@inject("localization_helper", "\App\Services\Localization\LocalizationHelper")

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    {{--<meta name="viewport"--}}
      {{--content="width=1500">--}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @env('production')
      <meta name="google-site-verification" content="nKGCWZGn1TsSjWNH8Hgk4Mr2pVmfRhC6jqn8M03NTOQ" />
    @endenv
    <title>{{ isset($title) ? $title : __("Vincoin Cash | The Easiest Way to Enter into Crypto!") }}</title>
    {{--TODO: add text here--}}
    {{--<meta name="description" content="">--}}
    <link rel="shortcut icon" type="image/png" href="/vnc_fav.png"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="alternate" hreflang="en" href="{{ $localization_helper->getLocalizedUrl() }}" />
    <link rel="alternate" hreflang="ru" href="{{ $localization_helper->getLocalizedUrl("ru") }}" />
    <link rel="alternate" hreflang="vn" href="{{ $localization_helper->getLocalizedUrl("vn") }}" />

    @stack('before_styles')

    <!-- Base CSS -->
    <link rel="stylesheet" href="{{ asset("/css/app.css") }}">

    @stack('after_styles')

    {{--<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="b64b788a-ac9f-4e5c-a7eb-d36f891229ba";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>--}}

    @env('production')
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112306604-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-112306604-1');
  </script>

  <!-- Yandex.Metrika counter -->
  <script type="text/javascript">
    (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
    try {
    w.yaCounter49241938 = new Ya.Metrika2({
    id:49241938,
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true,
    webvisor:true
    });
    } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/tag.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/49241938" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    @endenv
</head>
<body>

    @yield("content")

    <footer class="footer">
        <div class="section-container container mx-auto relative">
            <div class="footer__top">
                <div class="footer__top__item">
                    <div class="border-b border-white mb-3 py-2">
                        <h3 class="heading heading--sm text-white">@lang("navigation.main.Navigation")</h3>
                    </div>
                    {{--TODO: Add anchors to links, move to block.--}}
                    <ul class="list-reset">
                        <li class="mb-3"><a href="{{ route("site.home") }}#block-idea" class="nav__link">@lang("navigation.main.Get started")</a></li>
                        <li class="mb-3"><a href="{{ route("site.home") }}#block-mission" class="nav__link">@lang("navigation.main.Mission")</a></li>
                        <li class="mb-3"><a href="{{ route("site.home") }}#section-roadmap" class="nav__link">@lang("navigation.main.Road map")</a></li>
                    </ul>
                </div>
                <div class="footer__top__item">
                    <div class="border-b border-white mb-3 py-2">
                        <h3 class="heading heading--sm text-white">@lang('navigation.main.Services')</h3>
                    </div>
                    <ul class="list-reset">
                        <li class="mb-3"><a href="{{ route("site.home") }}#section-choose" class="nav__link">@lang('navigation.main.Trade Vincoin Cash')</a></li>
                        <li class="mb-3"><a href="{{ route("site.home") }}#section-choose" class="nav__link">@lang('navigation.main.Mine Vincoin Cash')</a></li>
                        <li class="mb-3"><a href="{{ asset(config('admin.white-paper_url')) }}" class="nav__link" target="_blank">@lang('navigation.main.White Paper')</a></li>
                        <li class="mb-3"><a href="https://chain.vincoin.cash/" class="nav__link" target="_blank">@lang('navigation.main.Block explorer')</a></li>
                        <li class="mb-3"><a href="{{ route('site.smartContract') }}" class="nav__link">@lang('Smart Contract')</a></li>
                        <li class="mb-3"><a href="{{ route("site.home") }}#section-other" class="nav__link">@lang('navigation.main.Faq')</a></li>
                    </ul>
                </div>
                <div class="footer__top__item">
                    <div class="border-b border-white mb-3 py-2">
                        <h3 class="heading heading--sm text-white">@lang("navigation.main.Information")</h3>
                    </div>
                    <ul class="list-reset">
                        <li class="mb-3"><a href="javascript:void(0);" class="nav__link" data-src="#contact-us-modal" data-fancybox>@lang('navigation.main.Contact Us')</a></li>
                        <li class="mb-3"><a href="mailto:{{ config("admin.support_mail") }}" class="nav__link">@lang('navigation.main.Write to support')</a></li>
                        {{--<li class="mb-3"><a href="#" class="nav__link">@lang("navigation.main.Online chat")</a></li>--}}
                        {{--<li class="mb-3"><a href="#" class="nav__link">Wiki</a></li>--}}
                    </ul>
                </div>
                <div class="footer__top__item__btns">
                    <button class="btn btn--white mb-6" type="button">
                      <a class="" href="{{ route("site.home") }}#section-wallet">@lang('Start mining')</a>
                    </button>
                    <button class="btn btn--white btn--disabled" type="button" disabled="disabled">@lang('Exchanges')</button>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="logo-wrapper">
                  <a href="{{ route('site.home') }}" class="logo logo--white">
                    <span class="logo__brand-img"></span>
                    <span class="logo__brand-name">Vincoin Cash</span>
                  </a>
                </div>
                <div class="ml-auto sm:mx-auto">
                    <div class="social-links">
                      <div class="social-link icon-linked_In"><a class="" href="https://www.linkedin.com/company/18756174/admin/updates/" target="_blank">
                          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                            <path data-name="-e-ico_linkedin" d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-5.12 30.43h-5.13V12.492h5.13V30.43zm-2.41-19.038a2.4 2.4 0 1 1 2.41-2.4 2.4 2.4 0 0 1-2.41 2.4zM33.31 30.43h-5.12V19.339c0-1.3-.37-2.208-1.97-2.208a3.075 3.075 0 0 0-3.16 2.208V30.43h-5.12V12.492h5.12v1.714a8.626 8.626 0 0 1 5.13-1.711c1.66 0 5.12 1 5.12 7V30.43z" fill-rule="evenodd"/>
                          </svg>
                        </a></div>
                        <div class="social-link icon-telegram"><a class="" href="https://t.me/vincoincash" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                                    <path fill-rule="evenodd" d="M20.5 40.502C9.453 40.502.497 31.547.497 20.5.497 9.453 9.453.498 20.5.498S40.502 9.453 40.502 20.5c0 11.047-8.955 20.002-20.002 20.002zm9.208-30.415l-23.979 9.25a.689.689 0 0 0 .007 1.288l5.842 2.182 2.262 7.272a.688.688 0 0 0 1.093.329l3.257-2.655a.971.971 0 0 1 1.184-.033l5.874 4.265a.689.689 0 0 0 1.079-.416l4.303-20.7a.688.688 0 0 0-.922-.782zM15.711 23.688c-.331.307-.545.72-.606 1.168l-.32 2.379c-.043.318-.489.349-.577.042l-1.234-4.338a1.15 1.15 0 0 1 .502-1.294l11.419-7.032c.205-.127.416.151.24.314l-9.424 8.761z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-facebook"><a class="" href="https://www.facebook.com/vincoincash/" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                                    <path fill-rule="evenodd" d="M19.999.004C8.974.004.004 8.974.004 19.999c0 11.024 8.97 19.995 19.995 19.995s19.994-8.971 19.994-19.995C39.993 8.974 31.024.004 19.999.004zm4.972 20.699h-3.252v11.595h-4.821V20.703h-2.291v-4.098h2.291v-2.65c0-1.899.902-4.865 4.865-4.865l3.57.013v3.978h-2.592c-.423 0-1.022.211-1.022 1.117v2.408h3.673l-.421 4.097z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-youtube"><a class="" href="https://www.youtube.com/channel/UCJqRWW07kX3m-I8IkRNUa2Q" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                                    <path fill-rule="evenodd" d="M20 40.001C8.954 40.001-.001 31.047-.001 20-.001 8.954 8.954-.001 20-.001 31.047-.001 40.001 8.954 40.001 20c0 11.047-8.954 20.001-20.001 20.001zM15.537 6.782l-1.031 3.964-1.073-3.964h-1.59c.319.936.65 1.876.969 2.813.485 1.407.787 2.468.924 3.193v4.087h1.511v-4.087l1.818-6.006h-1.528zm5.566 5.025c0-.805-.137-1.392-.422-1.772-.377-.515-.907-.772-1.59-.772-.679 0-1.21.257-1.587.772-.29.38-.426.967-.426 1.772v2.633c0 .799.136 1.393.426 1.769.377.513.908.77 1.587.77.683 0 1.213-.257 1.59-.77.285-.376.422-.969.422-1.769v-2.633zm5.04-2.453h-1.362v5.749c-.302.427-.589.638-.862.638-.182 0-.289-.108-.319-.319-.017-.045-.017-.211-.017-.53V9.354h-1.358v5.948c0 .53.045.89.12 1.118.137.38.44.559.879.559.497 0 1.014-.302 1.557-.924v.82h1.362V9.354zm4.629 10.812c-.273-1.186-1.244-2.062-2.412-2.192-2.768-.309-5.568-.311-8.356-.309-2.789-.002-5.59 0-8.357.309-1.168.13-2.138 1.006-2.411 2.192-.389 1.69-.394 3.535-.394 5.275 0 1.74 0 3.585.389 5.275.273 1.186 1.243 2.062 2.412 2.192 2.767.309 5.568.311 8.356.309 2.789.002 5.589 0 8.356-.309 1.168-.13 2.139-1.006 2.412-2.192.389-1.69.391-3.535.391-5.275 0-1.74.002-3.585-.386-5.275zm-1.621 9.188c-.373.541-.926.807-1.631.807s-1.243-.253-1.632-.762c-.286-.373-.433-.959-.433-1.75v-2.607c0-.795.13-1.377.416-1.754.39-.509.926-.763 1.616-.763.677 0 1.213.254 1.59.763.283.377.417.959.417 1.754v1.541h-2.697v1.32c0 .689.226 1.032.69 1.032.331 0 .525-.18.602-.54.013-.074.03-.374.03-.914h1.375v.196c0 .435-.015.735-.028.869a1.914 1.914 0 0 1-.315.808zm-2.353-3.894h1.349v-.688c0-.689-.226-1.033-.672-1.033-.451 0-.677.344-.677 1.033v.688zm-3.606 4.701c-.48 0-.943-.266-1.394-.823v.721h-1.349V20.07h1.349v3.262c.435-.536.898-.807 1.394-.807.536 0 .898.283 1.077.841.09.299.137.791.137 1.495v2.964c0 .689-.047 1.185-.137 1.5-.18.554-.541.836-1.077.836zm-.135-5.406c0-.672-.198-1.016-.587-1.016-.221 0-.447.105-.672.328v4.536c.225.226.451.332.672.332.389 0 .587-.332.587-1.004v-3.176zm-5.124 4.492c-.536.614-1.045.914-1.541.914-.434 0-.734-.176-.869-.553-.074-.227-.119-.582-.119-1.108v-5.884h1.349v5.48c0 .316 0 .48.012.523.032.21.135.316.316.316.27 0 .553-.208.852-.631v-5.688h1.35v7.443h-1.35v-.812zm-4.34.812h-1.501v-8.582h-1.585V20.07h4.701v1.407h-1.615v8.582zm5.498-14.318c-.44 0-.651-.348-.651-1.045v-3.163c0-.696.211-1.043.651-1.043.439 0 .65.347.65 1.043v3.163c0 .696-.211 1.045-.65 1.045z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-instagram"><a class="" href="https://www.instagram.com/vincoincash/" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                                    <path fill-rule="evenodd" d="M19.999 39.994C8.974 39.994.004 31.023.004 19.999.004 8.974 8.974.004 19.999.004s19.995 8.97 19.995 19.995c0 11.024-8.97 19.995-19.995 19.995zM31.37 17.665v-4.642a4.4 4.4 0 0 0-4.396-4.395H13.023a4.4 4.4 0 0 0-4.395 4.395v13.953a4.4 4.4 0 0 0 4.395 4.396h13.952a4.4 4.4 0 0 0 4.395-4.396v-9.311zm-4.397 11.488H13.022a2.178 2.178 0 0 1-2.179-2.177v-9.311h3.394a6.184 6.184 0 0 0-.458 2.334 6.227 6.227 0 0 0 6.22 6.219 6.226 6.226 0 0 0 6.219-6.219c0-.825-.167-1.614-.461-2.334h3.396v9.311a2.179 2.179 0 0 1-2.18 2.177zm-2.09-17.894l3.353-.01.503-.001v3.854l-3.842.013-.014-3.856zm-4.885 12.742a4.007 4.007 0 0 1-4.003-4.003 3.987 3.987 0 0 1 .757-2.333 4 4 0 0 1 3.247-1.669c1.335 0 2.517.661 3.246 1.669.472.658.757 1.463.757 2.334a4.009 4.009 0 0 1-4.004 4.002z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-twitter"><a class="" href="https://twitter.com/Vincoin_Cash" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                                    <path fill-rule="evenodd" d="M19.999.004C8.974.004.004 8.974.004 19.999c0 11.024 8.97 19.995 19.995 19.995s19.995-8.971 19.995-19.995c0-11.025-8.97-19.995-19.995-19.995zm8.92 15.418c.008.199.013.399.013.598 0 6.082-4.628 13.093-13.095 13.093a13.02 13.02 0 0 1-7.054-2.067c.36.042.727.064 1.098.064 2.157 0 4.14-.736 5.715-1.97a4.605 4.605 0 0 1-4.298-3.196 4.617 4.617 0 0 0 2.077-.078 4.605 4.605 0 0 1-3.691-4.512l.001-.058c.62.344 1.329.552 2.084.575a4.598 4.598 0 0 1-2.047-3.829c0-.845.227-1.636.623-2.316a13.063 13.063 0 0 0 9.485 4.81 4.603 4.603 0 0 1 7.842-4.198 9.195 9.195 0 0 0 2.923-1.117 4.615 4.615 0 0 1-2.024 2.546 9.148 9.148 0 0 0 2.643-.724 9.304 9.304 0 0 1-2.295 2.379z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-reddit"><a class="" href="https://www.reddit.com/user/Vincoin_cash/" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
                                    <path fill-rule="evenodd" d="M20 40.001C8.954 40.001 0 31.046 0 20 0 8.954 8.954-.001 20-.001S40.001 8.954 40.001 20c0 11.046-8.955 20.001-20.001 20.001zm11.134-23.433c-.828 0-1.595.334-2.182.9-2.159-1.428-5.082-2.339-8.318-2.455l1.768-5.589 4.785 1.127c-.001.024-.007.045-.007.07a2.592 2.592 0 0 0 2.59 2.589 2.593 2.593 0 0 0 2.589-2.589 2.592 2.592 0 0 0-2.589-2.589c-1.096 0-2.03.687-2.407 1.65l-5.159-1.214a.443.443 0 0 0-.525.297l-1.97 6.233c-3.385.04-6.45.956-8.704 2.426a3.162 3.162 0 0 0-2.139-.855 3.168 3.168 0 0 0-3.165 3.166c0 1.131.607 2.163 1.568 2.727a5.646 5.646 0 0 0-.101 1.017c0 4.682 5.729 8.489 12.772 8.489 7.044 0 12.774-3.807 12.774-8.489 0-.325-.034-.644-.088-.959a3.14 3.14 0 0 0 1.674-2.785 3.17 3.17 0 0 0-3.166-3.167zm1.263 5.058c-.458-1.364-1.408-2.604-2.725-3.633a2.265 2.265 0 0 1 1.461-.538 2.283 2.283 0 0 1 2.28 2.28 2.26 2.26 0 0 1-1.016 1.891zm-8.05 2.166a1.885 1.885 0 0 1 0-3.768 1.885 1.885 0 0 1 0 3.768zm.226 3.077a.443.443 0 0 1-.001.627c-.95.95-2.44 1.411-4.557 1.411-.005 0-.009-.004-.015-.004s-.01.004-.016.004c-2.116 0-3.607-.461-4.556-1.411a.444.444 0 0 1 .627-.627c.775.775 2.059 1.152 3.929 1.152.006 0 .01.003.016.003s.01-.003.015-.003c1.871 0 3.156-.377 3.93-1.152a.445.445 0 0 1 .628 0zm-8.913-3.077a1.884 1.884 0 1 1 0-3.769 1.884 1.884 0 0 1 0 3.769zM29.769 8.918a1.704 1.704 0 0 1 0 3.406 1.706 1.706 0 0 1-1.704-1.703c0-.939.765-1.703 1.704-1.703zM7.51 21.559a2.271 2.271 0 0 1-.922-1.824 2.282 2.282 0 0 1 2.278-2.28c.512 0 1.004.18 1.404.494-1.325 1.022-2.286 2.253-2.76 3.61z"/>
                                </svg>
                            </a></div>
                      <div class="social-link-group icon-medium-group">
                        <div class="social-link icon-medium icon-medium__main">
                          <a class="" href="javascript:void(0);">
                            <svg class="" xmlns="http://www.w3.org/2000/svg" width="40" height="40"><path data-name="Фигура 1171" d="M20 0a20 20 0 1 0 20 20A19.994 19.994 0 0 0 20 0zm11.76 11.07l-1.83 1.81a.559.559 0 0 0-.21.53v13.18a.568.568 0 0 0 .21.53l1.83 1.8v.4h-9.23v-.4l1.86-1.8a.552.552 0 0 0 .18-.53V15.9L19.3 29.32h-.72L12.43 15.9v8.99a1.262 1.262 0 0 0 .34 1.04l2.47 2.99v.4h-7v-.4l2.46-2.99a1.192 1.192 0 0 0 .32-1.04V14.5a.906.906 0 0 0-.29-.77l-2.15-2.66v-.39h6.82l5.22 11.59 4.65-11.59h6.49v.39z" fill-rule="evenodd"/></svg>
                          </a>
                        </div>
                        <div class="links-hidden">
                          <div class="social-link icon-medium icon-medium__en">
                            <a class="" href="https://medium.com/@vincoincash_eng" target="_blank">
                              <svg class="" xmlns="http://www.w3.org/2000/svg" width="41" height="41"><path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-3.48 19.01v2.37H11.1v2.87a1.3 1.3 0 0 0 .23.86 1.032 1.032 0 0 0 .78.26h5.71l.15 2.41c-1.35.14-3.65.22-6.9.22a3.332 3.332 0 0 1-2.36-.81A2.875 2.875 0 0 1 7.8 25v-9a2.875 2.875 0 0 1 .91-2.19 3.332 3.332 0 0 1 2.36-.81c3.25 0 5.55.08 6.9.22l-.15 2.43h-5.71a1.032 1.032 0 0 0-.78.26 1.259 1.259 0 0 0-.23.84v2.26h5.92zm15.85 7.74a1.03 1.03 0 0 1-1.16 1.18h-2.04a1.207 1.207 0 0 1-.78-.22 2.232 2.232 0 0 1-.54-.75l-3.9-7.92a13.077 13.077 0 0 1-.9-2.31h-.24a17.292 17.292 0 0 1 .15 2.37v8.83h-3.12V14.25a1.04 1.04 0 0 1 1.18-1.18h2.01a1.2 1.2 0 0 1 .77.22 2.133 2.133 0 0 1 .54.75l3.77 7.69c.36.72.72 1.51 1.08 2.39h.21c-.08-1.12-.13-1.95-.13-2.5v-8.55h3.1v13.68z" fill-rule="evenodd"/></svg>
                            </a>
                          </div>
                          <div class="social-link icon-medium icon-medium__vn">
                            <a class="" href="https://medium.com/@vincoincash_vn" target="_blank">
                              {{--<svg xmlns="http://www.w3.org/2000/svg" width="41" height="41"><path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-2.1 28.18h-3.75L9.99 13.33h3.07l3.47 11.63L20 13.33h3.05zm9.11 0h-2.97V17.4h2.97v10.78zm.06-12.49a.345.345 0 0 1-.38.4H24.9a.433.433 0 0 1-.31-.11.4.4 0 0 1-.11-.29v-1.97a.371.371 0 0 1 .42-.42h2.29a.349.349 0 0 1 .28.12.436.436 0 0 1 .1.3v1.97z" fill-rule="evenodd"/></svg>--}}
                              <svg xmlns="http://www.w3.org/2000/svg" width="41" height="41">
                                <path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-5.69 27.964H11.1L6.5 13.277h3.04l3.43 11.512 3.43-11.512h3.02zm17.77 0h-2.36l-6.14-9.685v9.685h-2.95V13.277h2.36l6.15 9.686v-9.686h2.94v14.687z" fill-rule="evenodd"/>
                              </svg>
                            </a>
                          </div>
                          <div class="social-link icon-medium icon-medium__ru">
                            <a class="" href="https://medium.com/@vincoincash_ru" target="_blank">
                              <svg class="" xmlns="http://www.w3.org/2000/svg" width="41" height="41"><path d="M13.53 15.39l-2.37.03v4.17h2.37a2.625 2.625 0 0 0 1.77-.44 2.281 2.281 0 0 0 .47-1.67 2.229 2.229 0 0 0-.46-1.65 2.659 2.659 0 0 0-1.78-.44zM20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-4.32 27.93l-1.12-4.54a1.817 1.817 0 0 0-.57-.98 1.673 1.673 0 0 0-1.02-.27l-2.31-.02v5.81H7.93V13.07c1.34-.13 3.22-.2 5.67-.2a7.206 7.206 0 0 1 4.24.95c.83.63 1.25 1.78 1.25 3.45a4.35 4.35 0 0 1-.66 2.56 2.99 2.99 0 0 1-2.19 1.12v.11a2.7 2.7 0 0 1 2.12 2.24l1.29 4.63h-3.47zm17.38-5.92q0 3.405-1.3 4.8c-.87.94-2.37 1.4-4.49 1.4-2.24 0-3.82-.46-4.73-1.4s-1.37-2.53-1.37-4.8v-8.94h3.28v8.94a4.911 4.911 0 0 0 .57 2.8 3.287 3.287 0 0 0 4.18 0 4.911 4.911 0 0 0 .57-2.8v-8.94h3.29v8.94z" fill-rule="evenodd"/></svg>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="footer__copyright"></div>
                </div>
            </div>
        </div>
    </footer>

    <div class="notification-group right-corner">
      <div class="notification telegram-support hidden">
        <a href="https://t.me/vincoincash" class="notification__wrap" target="_blank">
          <span class="notification__body">Telegram live support</span>
          <span class="notification__img"></span>
        </a>
      </div>
      <div class="scroll-up-btn hidden"></div>
    </div>

    {{--Contact Us modal window--}}
    <div class="contact-us js-form-container" style="display: none;" id="contact-us-modal">
        <div class="js-form-content tabs contact-us__tabs">
            <h2 class="contact-us__header">@lang("Contact us")</h2>
            <ul class="tabs__controls" role="tablist">
              <li class="tabs__controls__item tabs__controls__item-left active" role="tab"><a href="#tab-1" class="link">@lang("Send us a message")</a></li>
              <li class="tabs__controls__item tabs__controls__item-right"><a href="#tab-2" class="link" role="tab">@lang("Contact information")</a></li>
            </ul>
            <div class="contact-us__body tabs__container">
                <div id="tab-1" class="contact-us__body__col-form tab active" role="tabpanel">
                    <h3 class="heading heading--sm text-white mb-4 lg:hidden">@lang("Send us a message")</h3>
                    <form action="{{ route('site.contactUsProcess') }}" class="vin-form contact-us-form" method="POST">
                        <div class="form__holder">
                            <div class="form-group">
                                <label for="" class="form-label">
                                    @lang("Name")
                                </label>
                                <input type="text" name="name" class="form-control" placeholder="@lang("Your name")" required>
                                <div class="help-block" data-for="name"></div>
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label">
                                    E-mail
                                </label>
                                <input type="text" name="email" class="form-control" placeholder="your@mail.com" required>
                                <div class="help-block" data-for="email"></div>
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label">
                                    @lang("Message")
                                </label>
                                <textarea class="form-control" name="message" id="" cols="30" rows="4" placeholder="@lang("Message here...")" required></textarea>
                                <div class="help-block" data-for="message"></div>
                            </div>
                            <div class="form-group form-btns">
                                <button class="btn btn--white submit-btn ladda-button" data-style="zoom-in" data-spinner-color="#757575"><span class="ladda-label">@lang("Send")</span></button>
                                <button data-fancybox-close class="btn btn--transparent normal-case hidden lg:block" type="button">@lang("Close form")</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="separator"></div>
                <div id="tab-2" class="contact-us__body__col-info tab" role="tabpanel">
                    <h3 class="heading heading--sm text-white mb-4 lg:hidden">@lang("Contact information")</h3>
                    <div class="font-normal text-white text-xs tracking-wide text-left mb-1">@lang("Social network")</div>
                    <div class="social-links social-links--sm mb-6">
                        <div class="social-link icon-linked_In"><a class="" href="https://www.linkedin.com/company/18756174/admin/updates/" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" version="1.1" viewBox="0 0 41 41">
                              <path data-name="-e-ico_linkedin" d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-5.12 30.43h-5.13V12.492h5.13V30.43zm-2.41-19.038a2.4 2.4 0 1 1 2.41-2.4 2.4 2.4 0 0 1-2.41 2.4zM33.31 30.43h-5.12V19.339c0-1.3-.37-2.208-1.97-2.208a3.075 3.075 0 0 0-3.16 2.208V30.43h-5.12V12.492h5.12v1.714a8.626 8.626 0 0 1 5.13-1.711c1.66 0 5.12 1 5.12 7V30.43z" fill-rule="evenodd"/>
                            </svg>
                          </a></div>
                        <div class="social-link icon-telegram"><a class="" href="https://t.me/vincoincash" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 41 41">
                                    {{--<svg xmlns="http://www.w3.org/2000/svg" version="1.1">--}}
                                    <path fill-rule="evenodd" d="M20.5 40.502C9.453 40.502.497 31.547.497 20.5.497 9.453 9.453.498 20.5.498S40.502 9.453 40.502 20.5c0 11.047-8.955 20.002-20.002 20.002zm9.208-30.415l-23.979 9.25a.689.689 0 0 0 .007 1.288l5.842 2.182 2.262 7.272a.688.688 0 0 0 1.093.329l3.257-2.655a.971.971 0 0 1 1.184-.033l5.874 4.265a.689.689 0 0 0 1.079-.416l4.303-20.7a.688.688 0 0 0-.922-.782zM15.711 23.688c-.331.307-.545.72-.606 1.168l-.32 2.379c-.043.318-.489.349-.577.042l-1.234-4.338a1.15 1.15 0 0 1 .502-1.294l11.419-7.032c.205-.127.416.151.24.314l-9.424 8.761z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-facebook"><a class="" href="https://www.facebook.com/vincoincash/" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" version="1.1" viewBox="0 0 41 41">
                                    <path fill-rule="evenodd" d="M19.999.004C8.974.004.004 8.974.004 19.999c0 11.024 8.97 19.995 19.995 19.995s19.994-8.971 19.994-19.995C39.993 8.974 31.024.004 19.999.004zm4.972 20.699h-3.252v11.595h-4.821V20.703h-2.291v-4.098h2.291v-2.65c0-1.899.902-4.865 4.865-4.865l3.57.013v3.978h-2.592c-.423 0-1.022.211-1.022 1.117v2.408h3.673l-.421 4.097z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-youtube"><a class="" href="https://www.youtube.com/channel/UCJqRWW07kX3m-I8IkRNUa2Q" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" version="1.1" viewBox="0 0 41 41">
                                    <path fill-rule="evenodd" d="M20 40.001C8.954 40.001-.001 31.047-.001 20-.001 8.954 8.954-.001 20-.001 31.047-.001 40.001 8.954 40.001 20c0 11.047-8.954 20.001-20.001 20.001zM15.537 6.782l-1.031 3.964-1.073-3.964h-1.59c.319.936.65 1.876.969 2.813.485 1.407.787 2.468.924 3.193v4.087h1.511v-4.087l1.818-6.006h-1.528zm5.566 5.025c0-.805-.137-1.392-.422-1.772-.377-.515-.907-.772-1.59-.772-.679 0-1.21.257-1.587.772-.29.38-.426.967-.426 1.772v2.633c0 .799.136 1.393.426 1.769.377.513.908.77 1.587.77.683 0 1.213-.257 1.59-.77.285-.376.422-.969.422-1.769v-2.633zm5.04-2.453h-1.362v5.749c-.302.427-.589.638-.862.638-.182 0-.289-.108-.319-.319-.017-.045-.017-.211-.017-.53V9.354h-1.358v5.948c0 .53.045.89.12 1.118.137.38.44.559.879.559.497 0 1.014-.302 1.557-.924v.82h1.362V9.354zm4.629 10.812c-.273-1.186-1.244-2.062-2.412-2.192-2.768-.309-5.568-.311-8.356-.309-2.789-.002-5.59 0-8.357.309-1.168.13-2.138 1.006-2.411 2.192-.389 1.69-.394 3.535-.394 5.275 0 1.74 0 3.585.389 5.275.273 1.186 1.243 2.062 2.412 2.192 2.767.309 5.568.311 8.356.309 2.789.002 5.589 0 8.356-.309 1.168-.13 2.139-1.006 2.412-2.192.389-1.69.391-3.535.391-5.275 0-1.74.002-3.585-.386-5.275zm-1.621 9.188c-.373.541-.926.807-1.631.807s-1.243-.253-1.632-.762c-.286-.373-.433-.959-.433-1.75v-2.607c0-.795.13-1.377.416-1.754.39-.509.926-.763 1.616-.763.677 0 1.213.254 1.59.763.283.377.417.959.417 1.754v1.541h-2.697v1.32c0 .689.226 1.032.69 1.032.331 0 .525-.18.602-.54.013-.074.03-.374.03-.914h1.375v.196c0 .435-.015.735-.028.869a1.914 1.914 0 0 1-.315.808zm-2.353-3.894h1.349v-.688c0-.689-.226-1.033-.672-1.033-.451 0-.677.344-.677 1.033v.688zm-3.606 4.701c-.48 0-.943-.266-1.394-.823v.721h-1.349V20.07h1.349v3.262c.435-.536.898-.807 1.394-.807.536 0 .898.283 1.077.841.09.299.137.791.137 1.495v2.964c0 .689-.047 1.185-.137 1.5-.18.554-.541.836-1.077.836zm-.135-5.406c0-.672-.198-1.016-.587-1.016-.221 0-.447.105-.672.328v4.536c.225.226.451.332.672.332.389 0 .587-.332.587-1.004v-3.176zm-5.124 4.492c-.536.614-1.045.914-1.541.914-.434 0-.734-.176-.869-.553-.074-.227-.119-.582-.119-1.108v-5.884h1.349v5.48c0 .316 0 .48.012.523.032.21.135.316.316.316.27 0 .553-.208.852-.631v-5.688h1.35v7.443h-1.35v-.812zm-4.34.812h-1.501v-8.582h-1.585V20.07h4.701v1.407h-1.615v8.582zm5.498-14.318c-.44 0-.651-.348-.651-1.045v-3.163c0-.696.211-1.043.651-1.043.439 0 .65.347.65 1.043v3.163c0 .696-.211 1.045-.65 1.045z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-instagram"><a class="" href="https://www.instagram.com/vincoincash/" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" version="1.1" viewBox="0 0 41 41">
                                    <path fill-rule="evenodd" d="M19.999 39.994C8.974 39.994.004 31.023.004 19.999.004 8.974 8.974.004 19.999.004s19.995 8.97 19.995 19.995c0 11.024-8.97 19.995-19.995 19.995zM31.37 17.665v-4.642a4.4 4.4 0 0 0-4.396-4.395H13.023a4.4 4.4 0 0 0-4.395 4.395v13.953a4.4 4.4 0 0 0 4.395 4.396h13.952a4.4 4.4 0 0 0 4.395-4.396v-9.311zm-4.397 11.488H13.022a2.178 2.178 0 0 1-2.179-2.177v-9.311h3.394a6.184 6.184 0 0 0-.458 2.334 6.227 6.227 0 0 0 6.22 6.219 6.226 6.226 0 0 0 6.219-6.219c0-.825-.167-1.614-.461-2.334h3.396v9.311a2.179 2.179 0 0 1-2.18 2.177zm-2.09-17.894l3.353-.01.503-.001v3.854l-3.842.013-.014-3.856zm-4.885 12.742a4.007 4.007 0 0 1-4.003-4.003 3.987 3.987 0 0 1 .757-2.333 4 4 0 0 1 3.247-1.669c1.335 0 2.517.661 3.246 1.669.472.658.757 1.463.757 2.334a4.009 4.009 0 0 1-4.004 4.002z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-twitter"><a class="" href="https://twitter.com/Vincoin_Cash" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" version="1.1" viewBox="0 0 41 41">
                                    <path fill-rule="evenodd" d="M19.999.004C8.974.004.004 8.974.004 19.999c0 11.024 8.97 19.995 19.995 19.995s19.995-8.971 19.995-19.995c0-11.025-8.97-19.995-19.995-19.995zm8.92 15.418c.008.199.013.399.013.598 0 6.082-4.628 13.093-13.095 13.093a13.02 13.02 0 0 1-7.054-2.067c.36.042.727.064 1.098.064 2.157 0 4.14-.736 5.715-1.97a4.605 4.605 0 0 1-4.298-3.196 4.617 4.617 0 0 0 2.077-.078 4.605 4.605 0 0 1-3.691-4.512l.001-.058c.62.344 1.329.552 2.084.575a4.598 4.598 0 0 1-2.047-3.829c0-.845.227-1.636.623-2.316a13.063 13.063 0 0 0 9.485 4.81 4.603 4.603 0 0 1 7.842-4.198 9.195 9.195 0 0 0 2.923-1.117 4.615 4.615 0 0 1-2.024 2.546 9.148 9.148 0 0 0 2.643-.724 9.304 9.304 0 0 1-2.295 2.379z"/>
                                </svg>
                            </a></div>
                        <div class="social-link icon-reddit"><a class="" href="https://www.reddit.com/user/Vincoin_cash/" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" version="1.1" viewBox="0 0 41 41">
                                    <path fill-rule="evenodd" d="M20 40.001C8.954 40.001 0 31.046 0 20 0 8.954 8.954-.001 20-.001S40.001 8.954 40.001 20c0 11.046-8.955 20.001-20.001 20.001zm11.134-23.433c-.828 0-1.595.334-2.182.9-2.159-1.428-5.082-2.339-8.318-2.455l1.768-5.589 4.785 1.127c-.001.024-.007.045-.007.07a2.592 2.592 0 0 0 2.59 2.589 2.593 2.593 0 0 0 2.589-2.589 2.592 2.592 0 0 0-2.589-2.589c-1.096 0-2.03.687-2.407 1.65l-5.159-1.214a.443.443 0 0 0-.525.297l-1.97 6.233c-3.385.04-6.45.956-8.704 2.426a3.162 3.162 0 0 0-2.139-.855 3.168 3.168 0 0 0-3.165 3.166c0 1.131.607 2.163 1.568 2.727a5.646 5.646 0 0 0-.101 1.017c0 4.682 5.729 8.489 12.772 8.489 7.044 0 12.774-3.807 12.774-8.489 0-.325-.034-.644-.088-.959a3.14 3.14 0 0 0 1.674-2.785 3.17 3.17 0 0 0-3.166-3.167zm1.263 5.058c-.458-1.364-1.408-2.604-2.725-3.633a2.265 2.265 0 0 1 1.461-.538 2.283 2.283 0 0 1 2.28 2.28 2.26 2.26 0 0 1-1.016 1.891zm-8.05 2.166a1.885 1.885 0 0 1 0-3.768 1.885 1.885 0 0 1 0 3.768zm.226 3.077a.443.443 0 0 1-.001.627c-.95.95-2.44 1.411-4.557 1.411-.005 0-.009-.004-.015-.004s-.01.004-.016.004c-2.116 0-3.607-.461-4.556-1.411a.444.444 0 0 1 .627-.627c.775.775 2.059 1.152 3.929 1.152.006 0 .01.003.016.003s.01-.003.015-.003c1.871 0 3.156-.377 3.93-1.152a.445.445 0 0 1 .628 0zm-8.913-3.077a1.884 1.884 0 1 1 0-3.769 1.884 1.884 0 0 1 0 3.769zM29.769 8.918a1.704 1.704 0 0 1 0 3.406 1.706 1.706 0 0 1-1.704-1.703c0-.939.765-1.703 1.704-1.703zM7.51 21.559a2.271 2.271 0 0 1-.922-1.824 2.282 2.282 0 0 1 2.278-2.28c.512 0 1.004.18 1.404.494-1.325 1.022-2.286 2.253-2.76 3.61z"/>
                                </svg>
                            </a></div>
                        <div class="social-link-group icon-medium-group">
                          <div class="social-link icon-medium icon-medium__main">
                            <a class="" href="javascript:void(0);">
                              <svg class="" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 41 41"><path data-name="Фигура 1171" d="M20 0a20 20 0 1 0 20 20A19.994 19.994 0 0 0 20 0zm11.76 11.07l-1.83 1.81a.559.559 0 0 0-.21.53v13.18a.568.568 0 0 0 .21.53l1.83 1.8v.4h-9.23v-.4l1.86-1.8a.552.552 0 0 0 .18-.53V15.9L19.3 29.32h-.72L12.43 15.9v8.99a1.262 1.262 0 0 0 .34 1.04l2.47 2.99v.4h-7v-.4l2.46-2.99a1.192 1.192 0 0 0 .32-1.04V14.5a.906.906 0 0 0-.29-.77l-2.15-2.66v-.39h6.82l5.22 11.59 4.65-11.59h6.49v.39z" fill-rule="evenodd"/></svg>
                            </a>
                          </div>
                          <div class="links-hidden">
                            <div class="social-link icon-medium icon-medium__en">
                              <a class="" href="https://medium.com/@vincoincash_eng" target="_blank">
                                <svg class="" xmlns="http://www.w3.org/2000/svg" width="41" height="41" viewBox="0 0 41 41"><path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-3.48 19.01v2.37H11.1v2.87a1.3 1.3 0 0 0 .23.86 1.032 1.032 0 0 0 .78.26h5.71l.15 2.41c-1.35.14-3.65.22-6.9.22a3.332 3.332 0 0 1-2.36-.81A2.875 2.875 0 0 1 7.8 25v-9a2.875 2.875 0 0 1 .91-2.19 3.332 3.332 0 0 1 2.36-.81c3.25 0 5.55.08 6.9.22l-.15 2.43h-5.71a1.032 1.032 0 0 0-.78.26 1.259 1.259 0 0 0-.23.84v2.26h5.92zm15.85 7.74a1.03 1.03 0 0 1-1.16 1.18h-2.04a1.207 1.207 0 0 1-.78-.22 2.232 2.232 0 0 1-.54-.75l-3.9-7.92a13.077 13.077 0 0 1-.9-2.31h-.24a17.292 17.292 0 0 1 .15 2.37v8.83h-3.12V14.25a1.04 1.04 0 0 1 1.18-1.18h2.01a1.2 1.2 0 0 1 .77.22 2.133 2.133 0 0 1 .54.75l3.77 7.69c.36.72.72 1.51 1.08 2.39h.21c-.08-1.12-.13-1.95-.13-2.5v-8.55h3.1v13.68z" fill-rule="evenodd"/></svg>
                              </a>
                            </div>
                            <div class="social-link icon-medium icon-medium__vn">
                              <a class="" href="https://medium.com/@vincoincash_vn" target="_blank">
                                {{--<svg xmlns="http://www.w3.org/2000/svg" width="41" height="41" viewBox="0 0 41 41"><path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-2.1 28.18h-3.75L9.99 13.33h3.07l3.47 11.63L20 13.33h3.05zm9.11 0h-2.97V17.4h2.97v10.78zm.06-12.49a.345.345 0 0 1-.38.4H24.9a.433.433 0 0 1-.31-.11.4.4 0 0 1-.11-.29v-1.97a.371.371 0 0 1 .42-.42h2.29a.349.349 0 0 1 .28.12.436.436 0 0 1 .1.3v1.97z" fill-rule="evenodd"/></svg>--}}
                                <svg xmlns="http://www.w3.org/2000/svg" width="41" height="41" viewBox="0 0 41 41">
                                  <path d="M20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-5.69 27.964H11.1L6.5 13.277h3.04l3.43 11.512 3.43-11.512h3.02zm17.77 0h-2.36l-6.14-9.685v9.685h-2.95V13.277h2.36l6.15 9.686v-9.686h2.94v14.687z" fill-rule="evenodd"/>
                                </svg>
                              </a>
                            </div>
                            <div class="social-link icon-medium icon-medium__ru">
                              <a class="" href="https://medium.com/@vincoincash_ru" target="_blank">
                                <svg class="" xmlns="http://www.w3.org/2000/svg" width="41" height="41" viewBox="0 0 41 41"><path d="M13.53 15.39l-2.37.03v4.17h2.37a2.625 2.625 0 0 0 1.77-.44 2.281 2.281 0 0 0 .47-1.67 2.229 2.229 0 0 0-.46-1.65 2.659 2.659 0 0 0-1.78-.44zM20.5 0A20.5 20.5 0 1 0 41 20.5 20.5 20.5 0 0 0 20.5 0zm-4.32 27.93l-1.12-4.54a1.817 1.817 0 0 0-.57-.98 1.673 1.673 0 0 0-1.02-.27l-2.31-.02v5.81H7.93V13.07c1.34-.13 3.22-.2 5.67-.2a7.206 7.206 0 0 1 4.24.95c.83.63 1.25 1.78 1.25 3.45a4.35 4.35 0 0 1-.66 2.56 2.99 2.99 0 0 1-2.19 1.12v.11a2.7 2.7 0 0 1 2.12 2.24l1.29 4.63h-3.47zm17.38-5.92q0 3.405-1.3 4.8c-.87.94-2.37 1.4-4.49 1.4-2.24 0-3.82-.46-4.73-1.4s-1.37-2.53-1.37-4.8v-8.94h3.28v8.94a4.911 4.911 0 0 0 .57 2.8 3.287 3.287 0 0 0 4.18 0 4.911 4.911 0 0 0 .57-2.8v-8.94h3.29v8.94z" fill-rule="evenodd"/></svg>
                              </a>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="font-normal text-white text-xs tracking-wide text-left mb-1">E-mail</div>
                    <div class="heading heading--xs text-white">Info@vincoin.cash</div>


                    <div class="mt-auto mx-auto mb-2"><button data-fancybox-close class="btn btn--transparent normal-case min-w-180" type="button">@lang("Close form")</button></div>
                </div>
            </div>
        </div>
        <div class="success-msg-wrapper js-form-success-msg">
          <h2 class="heading heading--md mx-auto mt-16 mb-6 text-white">@lang("Complete!")</h2>
          <p class="paragraph mb-8 text-white">@lang("Your message sent!")</p>
          <div class="success-msg__img"></div>
          <div class="text-center">
            <a data-fancybox-close href="javascript:void(0);" class="link text-white sm:hidden">@lang("Close form")</a>
            <button data-fancybox-close class="btn btn--transparent normal-case min-w-130 mx-auto hidden sm:block" type="button">@lang("Close form")</button>
          </div>
        </div>
    </div>

    @stack('before_scripts')

    <script>
        window.appLocale = '{{ \Illuminate\Support\Facades\App::getLocale() }}';
      {{--console.log('App Locale: ', '{{ \Illuminate\Support\Facades\App::getLocale() }}');--}}
      {{--console.log('Session Locale: ', '{{ session("pref_locale") }}');--}}
    </script>

    <script src="{{ asset("/js/app.js") }}"></script>

    @stack('after_scripts')
</body>
</html>
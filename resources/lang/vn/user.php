<?php

return [
    "User information" => "Thông tin người dùng",
    "Profile" => "Hồ sơ",
    "Social accounts" => "Tài khoản mạng xã hội",
    "Wallet number" => "Số tài khoản ví",
    "Add your wallet number:" => "Thêm số tài khoản ví:",
    "Referrer link" => "Liên kết giới thiệu",
    "Join our Telegram group:" => "Tham gia nhóm Telegram:",
    "Add your Telegram username:" => "Thêm tên tài khoản Telegram của bạn:",
    "Telegram nickname" => "Tên Telegram",
    "Your Telegram username" => "Tên tài khoản Telegram của bạn",
    "Subscribe to Facebook:" => "Theo dõi Facebook:",
    "Subscribe to Twitter:" => "Theo dõi Twitter:",
    "Account updated!" => "Tài khoản đã được cập nhật!",
    "Join" => "Tham gia",
    "Add" => "Thêm",
    "Earn up to" => "Nhận được tới",
    "Edit field" => "Mục cần sửa"
];

<?php

return [
    'main' => [
        'Navigation' => 'Chuyển hướng',
        'Menu' => 'Danh mục',
        'Close Menu' => 'Đóng danh mục',
        'Back' => 'Quay lại',
        'Get started' => 'Bắt đầu',
        'Mission' => 'Nhiệm vụ',
        'Road map' => 'Lộ trình',
        'About us' => 'Về chúng tôi',
        'Services' => 'Dịch vụ',
        'Buy Vincoin Cash' => 'Mua Vincoin Cash',
        'Trade Vincoin Cash' => 'Giao dịch Vincoin Cash',
        'Mine Vincoin Cash' => 'Đào Vincoin Cash',
        'White Paper' => 'White Paper',
        'Block explorer' => 'Block explorer',
        'Faq' => 'Faq',
        'Contact Us' => 'Liên hệ chúng tôi',
        'Sign In/Sign Up' => 'Đăng nhập/Đăng ký',
        'Sign In' => 'Đăng nhập',
        'Account' => 'Tài khoản',
        'Log out' => 'Đăng xuất',
        'Download' => 'Tải về',
        'Write to support' => 'Liên hệ hỗ trợ',
        'Information' => 'Thông tin',
        'Testnet is live' => 'Testnet is live'
    ]
];

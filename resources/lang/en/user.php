<?php

return [
    "User information" => "User information",
    "Profile" => "Profile",
    "Social accounts" => "Social accounts",
    "Wallet number" => "Wallet number",
    "Add your wallet number:" => "Add your wallet number:",
    "Referrer link" => "Referrer link",
    "Join our Telegram group:" => "Join our Telegram group:",
    "Add your Telegram username:" => "Add your Telegram username:",
    "Telegram nickname" => "Telegram nickname",
    "Your Telegram username" => "Your Telegram username",
    "Subscribe to Facebook:" => "Subscribe to Facebook:",
    "Subscribe to Twitter:" => "Subscribe to Twitter:",
    "Account updated!" => "Account updated!",
    "Join" => "Join",
    "Add" => "Add",
    "Earn up to" => "Earn up to",
    "Edit field" => "Edit field"
];

<?php

return [
    "User information" => "Информация пользователя",
    "Profile" => "Профиль",
    "Social accounts" => "Социальные группы",
    "Wallet number" => "Номер кошелька",
    "Add your wallet number:" => "Добавить номер кошелька:",
    "Referrer link" => "Партнерская ссылка",
    "Join our Telegram group:" => "Присоеденяйтесь к нашей Telegram группе:",
    "Add your Telegram username:" => "Добавить имя пользователя Telegram:",
    "Telegram nickname" => "Имя в Telegram",
    "Your Telegram username" => "Ваше имя в Telegram",
    "Subscribe to Facebook:" => "Подписаться на Facebook:",
    "Subscribe to Twitter:" => "Подписаться на Twitter:",
    "Account updated!" => "Аккаунт обновлен!",
    "Join" => "Перейти",
    "Add" => "Добавить",
    "Earn up to" => "Заработай до",
    "Edit field" => "Редактировать"
];

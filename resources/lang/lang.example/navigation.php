<?php

return [
    'main' => [
        'Navigation' => '',
        'Menu' => '',
        'Close Menu' => '',
        'Back' => '',
        'Get started' => '',
        'Mission' => '',
        'Road map' => '',
        'About us' => '',
        'Services' => '',
        'Buy Vincoin' => '',
        'Mine Vincoin' => '',
        'Faq' => '',
        'Contact Us' => '',
        'Sign In/Sign Up' => '',
        'Sign In' => '',
        'Account' => '',
        'Log out' => '',
        'Download' => '',
        'Write to support' => '',
        'Information' => ''
    ]
];


$(function(ClipboardJS) {
    const $header = $('.main-header');

    checkHeader(document, $header);

    $(document).on('scroll', function (event) {
        checkHeader(this, $header);
    });

    $(".social-expand-btn").on("click", function (e) {
        let socialLinks = $(".section-banner .social-links");
        if (socialLinks.css("opacity") === "0") {
            socialLinks.css("opacity", "1");
            socialLinks.css("visibility", "visible");
        } else {
            socialLinks.css("opacity", "0");
            socialLinks.css("visibility", "hidden");
        }
        $(this).toggleClass("is-active");
    });

    // $(".QR-code__copy-addr").on('click', function (e) {
    //     e.preventDefault();
    //     $(this).closest(".QR-code__zoom").data("address");
    // });

    // logo flip animation
    $('.logo-spin-wrapper').on({
        mouseenter: function () {
            //stuff to do on mouse enter
            let self = $(this);

            self.find('.logo-spin').animateCss('flipInY', function() {
                // Do somthing after animation
            });
        },
        mouseleave: function () {
            //stuff to do on mouse leave
        }
    });

    initDistChart();
    // initCanvasWave();

    // const mobileBreakpoint = 768;
    const mobileBreakpoint = 991;
    let iScreenWidth = document.body.getBoundingClientRect().width;

    initCardsSlider(iScreenWidth, mobileBreakpoint);
});

function initCardsSlider(iScreenWidth, mobileBreakpoint) {
    const sliders = document.querySelectorAll('.js-mobile-slider');
    let aFlickitySliders = [];

    function initFlickity(slider) {
        slider.style.display = 'block';
        return new Flickity( slider, {
            cellAlign: 'left',
            contain: true,
            pageDots: false,
            prevNextButtons: false
        });
    }

    // $(".js-mobile-slider").show();

    for(let i = 0; i < sliders.length; i += 1){
        const slider = sliders[i];
        if(iScreenWidth < mobileBreakpoint) {
            let oSlider = initFlickity(slider);
            aFlickitySliders.push(oSlider);
        }
    }

    window.addEventListener('resize', () => {
        iScreenWidth = document.body.getBoundingClientRect().width;

        for(let i = 0; i < sliders.length; i += 1){
            const oSlider = sliders[i];
            const oFlickSlider = aFlickitySliders[i];
            if(iScreenWidth < mobileBreakpoint){
                if(!oFlickSlider) {
                    let createdFlickSlider = initFlickity(oSlider);
                    aFlickitySliders.push(createdFlickSlider);
                }
            } else {
                if(oFlickSlider) {
                    oFlickSlider.destroy();
                    aFlickitySliders.splice(0, 1);
                }
            }
        }
    });

    // $(".js-mobile-slider").css('visibility', 'visible');
}

function initDistChart() {
    // based ready dom, initialize echarts instance
    var myChart = echarts.init(document.getElementById('coin-dist__chart'));
    let coinSupply = 8.6;

    // Specify configurations and data graphs
    var option = {
        backgroundColor: 'transparent',

        // title: {
        //     text: 'Customized Pie',
        //     left: 'center',
        //     top: 20,
        //     textStyle: {
        //         color: '#ccc'
        //     }
        // },

        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c}M ({d}%)"
        },

        visualMap: {
            show: false,
            min: 0,
            max: coinSupply,
            inRange: {
                colorLightness: [0.4, 0.7]
            }
        },
        series : [
            {
                name:'Coin distribution',
                type:'pie',
                radius : '75%',
                center: ['50%', '50%'],
                data:[
                    {value:(coinSupply * 0.15).toPrecision(3), name:'Team'},
                    {value:(coinSupply * 0.15).toPrecision(3), name:'Marketing'},
                    {value:(coinSupply * 0.15).toPrecision(3), name:'Development'},
                    {value:(coinSupply * 0.55).toPrecision(3), name:'Miners'},
                ].sort(function (a, b) { return a.value - b.value}),
                roseType: 'angle',
                label: {
                    normal: {
                        show: false,
                        // textStyle: {
                        //     color: 'rgba(0, 0, 0, 0.5)'
                        // }
                    }
                },
                // labelLine: {
                //     normal: {
                //         lineStyle: {
                //             color: 'rgba(0, 0, 0, 0.5)'
                //         },
                //         smooth: 0.2,
                //         length: 10,
                //         length2: 20
                //     }
                // },
                itemStyle: {
                    normal: {
                        color: '#c23531',
                        shadowBlur: 100,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

    // Use just the specified configurations and data charts.
    myChart.setOption(option);
}

function checkHeader(elem, $header) {
    if ($(elem).scrollTop() > 40 || $('body').hasClass("contact-us-active")) {
        $header.removeClass("main-header--transparent");
    } else {
        $header.addClass("main-header--transparent");
    }
}

$.fn.extend({
    animateCss: function(animationName, callback) {
        var animationEnd = (function(el) {
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        })(document.createElement('div'));

        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);

            if (typeof callback === 'function') callback();
        });

        return this;
    },
});

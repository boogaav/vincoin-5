
import FormHelper from '../utils/form-helper';

$(function() {
    const $authForm = $(".auth-form"),
          $resetPassForm = $('.reset-pass-form'),
          $registerCheck = $authForm.find('#register');

    $authForm.on('submit', function (event) {
        FormHelper.hideErrors($authForm);
        FormHelper.processingForm($authForm, true);
    });

    $resetPassForm.on('submit', function (event) {
        FormHelper.hideErrors($resetPassForm);
        FormHelper.processingForm($resetPassForm, true);
    });

    checkRegister($authForm, $registerCheck.is(":checked"));

    $registerCheck.on('click', function (event) {
        // event.preventDefault();
        checkRegister($authForm, $(this).is(":checked"));
    });
});

function checkRegister($form, isChecked) {
    if (isChecked) {
        FormHelper.showField($form, '#password-confirm');
        $("#auth-title").text("Register");
        $("#auth-submit .ladda-label").text("Register");
    } else {
        FormHelper.hideField($form, '#password-confirm');
        $("#auth-title").text("Login");
        $("#auth-submit .ladda-label").text("Login");
    }
}
